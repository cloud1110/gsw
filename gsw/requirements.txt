appdirs==1.4.3
Django==1.11
packaging==16.8
pyparsing==2.2.0
pytz==2017.2
six==1.10.0
django-allauth==0.31.0
python-openid==2.2.5
requests==2.13.0
requests-oauthlib==0.8.0
django-mailgun==0.9.1
django-bootstrap3==8.2.2
gevent==1.2.1
gunicorn==19.7.1