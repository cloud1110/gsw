from gevent import monkey
monkey.patch_all()

import multiprocessing

preload_app = True
bind = "127.0.0.1:8000"
workers = multiprocessing.cpu_count() * 2 + 1
worker_class = 'gevent'
worker_connections = 1000
access_log_format = "%(t)s \"%(r)s\" %(s)s %(b)s %(L)s"
accesslog = '/home/gsw/logs/gunicorn_access.log'
name = "gsw_app"
loglevel = "debug"


def post_fork(server, worker):
    from django.core.urlresolvers import resolve
    resolve('/')
