#!/bin/bash
 
NAME="gsw_app"                                  # Name of the application
DJANGODIR=/home/gsw/gsw/gsw            # Django project directory
DJANGO_SETTINGS_MODULE=gsw.settings             # which settings file should Django use
DJANGO_WSGI_MODULE=gsw.wsgi                     # WSGI module name
 
echo "Starting $NAME as `whoami`"
 
# Activate the virtual environment
cd $DJANGODIR
# source /home/gsw/env/bin/activate #not necessary
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH
  
# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec /home/gsw/env/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
    -c deploy/gunicorn_settings.py
