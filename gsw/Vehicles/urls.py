from django.conf.urls import url
from django.contrib.auth.decorators import login_required


from . import views

urlpatterns = [
    url(r'^$', login_required(views.VehiclesView.as_view()), name='vehicles')
]
