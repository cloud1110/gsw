from django.contrib import admin

from .models import Vehicle, Event


class VehicleAdmin(admin.ModelAdmin):
    list_display = ('registration_number', )


class EventAdmin(admin.ModelAdmin):
    list_display = ('vehicle', )


admin.site.register(Vehicle, VehicleAdmin)
admin.site.register(Event, EventAdmin)
