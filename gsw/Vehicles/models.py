from __future__ import unicode_literals

from django.db import models


class Vehicle(models.Model):
    registration_number = models.CharField(max_length=20)
    created = models.DateTimeField("vehicle created", auto_now_add=True)

    def __unicode__(self):
        return self.registration_number


class Event(models.Model):
    EVENT_CATEGORY = (
        ("0", "non-payment"),
    )
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    category = models.CharField("event type", choices=EVENT_CATEGORY, default="0", max_length=20)
    created = models.DateTimeField("event created time", auto_now_add=True)

