from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.views.generic.base import TemplateView
from .models import Vehicle


class VehiclesView(TemplateView):
    template_name = 'Vehicles/index.html'

    def get_context_data(self, **kwargs):
        context = super(VehiclesView, self).get_context_data(**kwargs)
        vehicle_list = Vehicle.objects.all()
        paginator = Paginator(vehicle_list, 20)
        page = self.request.GET.get('page')
        try:
            show_lines = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            show_lines = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            show_lines = paginator.page(paginator.num_pages)
        context['vehicle_list'] = show_lines
        return context
